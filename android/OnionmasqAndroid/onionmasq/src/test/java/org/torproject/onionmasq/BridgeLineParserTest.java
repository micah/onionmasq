package org.torproject.onionmasq;

import static org.junit.Assert.*;
import static org.torproject.onionmasq.BridgeLineParser.CERT;
import static org.torproject.onionmasq.BridgeLineParser.FINGERPRINT;
import static org.torproject.onionmasq.BridgeLineParser.FRONTS;
import static org.torproject.onionmasq.BridgeLineParser.IAT_MODE;
import static org.torproject.onionmasq.BridgeLineParser.ICE;
import static org.torproject.onionmasq.BridgeLineParser.URL;
import static org.torproject.onionmasq.BridgeLineParser.UTLS_IMITATE;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class BridgeLineParserTest {

    @Test
    public void getConfigFromBridgeLine_obfs4() {
        String obfs4 = "obfs4 146.57.248.225:22 10A6CD36A537FCE513A322361547444B393989F0 cert=K1gDtDAIcUfeLqbstggjIw2rtgIKqdIhUlHp82XRqNSq/mtAjp1BIC9vHKJ2FAEpGssTPw iat-mode=0";
        String obfs4_2 = "obfs4 146.57.248.225:22 10A6CD36A537FCE513A322361547444B393989F0   iat-mode=0    cert=K1gDtDAIcUfeLqbstggjIw2rtgIKqdIhUlHp82XRqNSq/mtAjp1BIC9vHKJ2FAEpGssTPw";

        BridgeLineParser.IPtConfig config = BridgeLineParser.getIPtConfigFrom(obfs4);
        assertNotNull(config);
        assertEquals("obfs4", config.getBridgeType());
        assertEquals("K1gDtDAIcUfeLqbstggjIw2rtgIKqdIhUlHp82XRqNSq/mtAjp1BIC9vHKJ2FAEpGssTPw", config.getOption(CERT));
        assertEquals("0", config.getOption(IAT_MODE));
        assertEquals(obfs4.trim(), config.getBridgeLines());

        config = BridgeLineParser.getIPtConfigFrom(obfs4_2);
        assertNotNull(config);
        assertEquals("obfs4", config.getBridgeType());
        assertEquals(1, config.getBridgeConfigs().size());
        assertEquals("K1gDtDAIcUfeLqbstggjIw2rtgIKqdIhUlHp82XRqNSq/mtAjp1BIC9vHKJ2FAEpGssTPw", config.getOption(CERT));
        assertEquals("0", config.getOption(IAT_MODE));
        assertEquals(obfs4_2.trim(), config.getBridgeLines());
    }

    @Test
    public void getConfigFromBridgeLine_snowflake() {
        List<String> snowflakeLines = Arrays.asList(
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ fronts=www.cdn77.com,www.phpmyadmin.net ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn",
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net",
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/",
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA",
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478",
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478");

        for (String line : snowflakeLines) {
            BridgeLineParser.IPtConfig config = BridgeLineParser.getIPtConfigFrom(line);
            assertNotNull(config);
            assertEquals("snowflake", config.getBridgeType());
            assertEquals(1, config.getBridgeConfigs().size());
            assertEquals("8838024498816A039FCBBAB14E6F40A0843051FA", config.getOption(FINGERPRINT));
            assertEquals("https://1098762253.rsc.cdn77.org/", config.getOption(URL));
            assertEquals("www.cdn77.com,www.phpmyadmin.net", config.getOption(FRONTS));
            assertEquals("stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478", config.getOption(ICE));
            assertEquals("hellorandomizedalpn", config.getOption(UTLS_IMITATE));
            assertEquals(line, config.getBridgeLines());
            assertNull(config.getOption(IAT_MODE));
            assertNull(config.getOption(CERT));
        }
    }

    @Test
    public void getConfigFromBridgeLine_multiline_snowflake() {
        String snowflakeLines =
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ fronts=www.cdn77.com,www.phpmyadmin.net ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn \n" +
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net  \n" +
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/   \n" +
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA \n" +
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478\n" +
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478";

        BridgeLineParser.IPtConfig config = BridgeLineParser.getIPtConfigFrom(snowflakeLines);
        assertNotNull(config);
        assertEquals(6, config.getBridgeConfigs().size());
        assertEquals(1, config.getBridgeLines().split("\n").length);
        assertNull(config.getOption(IAT_MODE));
        assertNull(config.getOption(CERT));
    }

    @Test
    public void getConfigFromBridgeLine_selectSingleBridgeConfig_snowflakeRandomized() {
        String snowflakeLines =
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ fronts=www.cdn77.com,www.phpmyadmin.net ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn \n" +
                        "snowflake 192.0.2.4:81 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net  \n" +
                        "snowflake 192.0.2.4:82 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/   \n" +
                        "snowflake 192.0.2.4:83 8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA \n" +
                        "snowflake 192.0.2.4:84 8838024498816A039FCBBAB14E6F40A0843051FA utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478\n" +
                        "snowflake 192.0.2.4:85 8838024498816A039FCBBAB14E6F40A0843051FA utls-imitate=hellorandomizedalpn fronts=www.cdn77.com,www.phpmyadmin.net url=https://1098762253.rsc.cdn77.org/ fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478";

        BridgeLineParser.IPtConfig config = BridgeLineParser.getIPtConfigFrom(snowflakeLines);
        assertNotNull(config);
        config.selectSingleBridgeConfig();
        String previousLine = config.getSelectedBridgeLine();
        boolean randomlySelected = false;
        for (int i = 0; i < 100; i++) {
            config.selectSingleBridgeConfig();
            String currentLine = config.getSelectedBridgeLine();
            if (!previousLine.equals(currentLine)) {
                randomlySelected = true;
                System.out.println("attempts: " + (i + 1));
                break;
            }
        }
        assertTrue(randomlySelected);
    }

    @Test
    public void getConfigFromBridgeLine_mixedTypes_BridgeTypeRandomlySelected() {
        String bridgeLines =
                "obfs4 146.57.248.225:22 10A6CD36A537FCE513A322361547444B393989F0 cert=K1gDtDAIcUfeLqbstggjIw2rtgIKqdIhUlHp82XRqNSq/mtAjp1BIC9vHKJ2FAEpGssTPw iat-mode=0\n" +
                "obfs4 146.57.248.225:22 10A6CD36A537FCE513A322361547444B393989F0   iat-mode=0    cert=K1gDtDAIcUfeLqbstggjIw2rtgIKqdIhUlHp82XRqNSq/mtAjp1BIC9vHKJ2FAEpGssTPw\n" +
                "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ fronts=www.cdn77.com,www.phpmyadmin.net ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn \n";

        BridgeLineParser.IPtConfig previousConfig = BridgeLineParser.getIPtConfigFrom(bridgeLines);

        boolean randomlySelected = false;
        for (int i = 0; i < 100; i++) {
            BridgeLineParser.IPtConfig currentConfig = BridgeLineParser.getIPtConfigFrom(bridgeLines);
            if (!currentConfig.getBridgeLines().equals(previousConfig.getBridgeLines())) {
                randomlySelected = true;
                System.out.println("attempts: " + (i + 1));
                break;
            }
        }

      assertTrue(randomlySelected);
    }
}