package org.torproject.onionmasq.circuit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.torproject.onionmasq.events.RelayDetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Vector;


public class CircuitTest {

    @Test
    public void testCreation() {
        Circuit circuit = new Circuit(null, null);
        assertNotNull(circuit.getRelayDetails());
    }

    @Test
    public void testEquals() {
        LinkedList<RelayDetails> list1 = new LinkedList<>();
        Vector<RelayDetails> list2 = new Vector<>();
        RelayDetails r1 = new RelayDetails();
        r1.addresses = new ArrayList<>(Arrays.asList("address1", "address2", "address3"));
        r1.ed_identity = "ed_identity";

        RelayDetails r2 = new RelayDetails();
        r2.addresses = new LinkedList<>(Arrays.asList("address1", "address2", "address3"));
        r2.ed_identity = "ed_identity";

        RelayDetails r3 = new RelayDetails();
        r3.addresses = new LinkedList<>(Arrays.asList("address1", "address2", "address3", "address4"));
        r3.ed_identity = "ed_identity";

        RelayDetails r4 = new RelayDetails();
        r4.addresses = new LinkedList<>(Arrays.asList("address1", "address2",  "address4"));
        r4.ed_identity = "ed_identity";

        list1.add(r1);
        list2.add(r2);
        assertEquals(new Circuit(list1, "torproject.org:443"), new Circuit(list2, "torproject.org:443"));
        assertEquals(new Circuit(list1, null), new Circuit(list2, null));
        assertNotEquals(new Circuit(list1, "torproject.org:443"), new Circuit(list2, "subdomain.torproject.org:443"));

        Circuit c1 = new Circuit(null, "torproject.org:443");
        Circuit c2 = new Circuit(null, "torproject.org:443");
        assertEquals(c1.getDestinationDomain(), c2.getDestinationDomain());

        c2 = new Circuit(null, "subdomain.torproject.org");
        assertNotEquals(c1.getDestinationDomain(), c2.getDestinationDomain());

        list2.clear();
        r2.ed_identity = null;
        list2.add(r2);
        assertNotEquals(new Circuit(list1, null), new Circuit(list2, null));

        list2.clear();
        list2.add(r3);
        assertNotEquals(new Circuit(list1, null), new Circuit(list2, null));

        list2.clear();
        list2.add(r4);
        assertNotEquals(new Circuit(list1, null), new Circuit(list2, null));

    }

    @Test
    public void testGetDestinationDomain() {
        Circuit c1 = new Circuit(null, "torproject.org:443");
        assertEquals("torproject.org", c1.getDestinationDomain());

        c1 = new Circuit(null, "subdomain.torproject.org:443");
        assertEquals("subdomain.torproject.org", c1.getDestinationDomain());

        c1 = new Circuit(null, "200.100.54.2:443");
        assertEquals("200.100.54.2", c1.getDestinationDomain());

        c1 = new Circuit(null, "subdomain.torproject.org");
        assertEquals("subdomain.torproject.org", c1.getDestinationDomain());
    }

}