package org.torproject.onionmasq.logging;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.concurrent.CountDownLatch;

@RunWith(RobolectricTestRunner.class)
public class LogObservableTest {

    LogObservable logObservable;
    @Before
    public void setup() {
        logObservable = LogObservable.getInstance();
        logObservable.setReverseOrder(false);
    }

    @After
    public void tearDown() {
        logObservable = null;
    }

    @Test
    public void testSetReverseOrder_existingList_reversedOrder() {
        LogObservable.getInstance().addLog("Log 1");
        LogObservable.getInstance().addLog("Log 2");
        LogObservable.getInstance().addLog("Log 3");

        assertEquals("Log 1", logObservable.getLogListData().getValue().get(0).content);
        LogObservable.getInstance().setReverseOrder(true);

        assertEquals("Log 3", LogObservable.getInstance().getLogListData().getValue().get(0).content);
    }


    @Test
    public void testSetReverseOrder_addToList_reversedOrder() {
        LogObservable.getInstance().setReverseOrder(true);

        LogObservable.getInstance().addLog("Log 1");
        LogObservable.getInstance().addLog("Log 2");
        LogObservable.getInstance().addLog("Log 3");

        assertEquals("Log 3", LogObservable.getInstance().getLogListData().getValue().get(0).content);
    }

    @Test
    public void testSetReverseOrder_multithreading_threadsafe() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(2);
        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                LogObservable.getInstance().addLog("Log " + (i+1));
            }
            countDownLatch.countDown();
        }).start();

        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                LogObservable.getInstance().setReverseOrder(i % 2 == 0);
            }
            countDownLatch.countDown();
        }).start();

        countDownLatch.await();
        assertEquals("Log 1", LogObservable.getInstance().getLogListData().getValue().get(0).content);
        assertEquals("Log 2", LogObservable.getInstance().getLogListData().getValue().get(1).content);
        assertEquals("Log 99", LogObservable.getInstance().getLogListData().getValue().get(98).content);
        assertEquals("Log 100", LogObservable.getInstance().getLogListData().getValue().get(99).content);
    }

}
