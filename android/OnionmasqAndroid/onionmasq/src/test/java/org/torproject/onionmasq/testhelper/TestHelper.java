package org.torproject.onionmasq.testhelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

public class TestHelper {

    public static String getStringFromResources(String file) throws IOException, NullPointerException {
        return getInputAsString(Objects.requireNonNull(TestHelper.class.getClassLoader()).getResourceAsStream(file));
    }
    private static String getInputAsString(InputStream fileAsInputStream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(fileAsInputStream));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            line = br.readLine();
            if (line != null) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }
}
