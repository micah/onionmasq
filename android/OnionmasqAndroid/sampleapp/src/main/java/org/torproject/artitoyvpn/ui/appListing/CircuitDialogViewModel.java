package org.torproject.artitoyvpn.ui.appListing;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import org.torproject.onionmasq.OnionMasq;
import org.torproject.onionmasq.circuit.Circuit;
import org.torproject.onionmasq.circuit.CircuitCountryCodes;
import org.torproject.onionmasq.events.RelayDetails;

import java.util.ArrayList;

public class CircuitDialogViewModel extends AndroidViewModel {
    public String circuitDetails = "";
    private final int appUID;

    public CircuitDialogViewModel(@NonNull Application application, int appUID) {
        super(application);
        this.appUID = appUID;
        init();
    }

    public void init() {
        ArrayList<Circuit> circuits = OnionMasq.getCircuitsForAppUid(appUID);
        ArrayList<CircuitCountryCodes> circuitCountryCodes = OnionMasq.getCircuitCountryCodesForAppUid(appUID);
        StringBuilder details = new StringBuilder();

        // print country codes
        details.append("~~~~~ CIRCUITS appUID ").append(appUID).append("~~~~~\n");
        int a = 0;
        for (CircuitCountryCodes circuitCountryCode : circuitCountryCodes) {
            int b = 0;
            a++;
            details.append("--- circuit ").append(a).append("\n");
            for (String countryCode : circuitCountryCode.getCountryCodes()) {
                b++;
                details.append("     hop ").append(b).append(": ").append(countryCode).append("\n");
            }
        }

        // print socket details
        int i = 0;
        for (Circuit circuit : circuits) {
            i++;
            details.append("~~~~~ SOCKET CONNECTION ").append(i).append("~~~~~\n");
            details.append("    ").append(circuit.getDestinationDomain()).append("\n");
            int k = 0;
            for (RelayDetails relay : circuit.getRelayDetails()) {
                k++;
                details.append("  relay ").append(k).append(":\n");
                for (String address : relay.addresses) {
                    details.append("    ").append(address).append("\n");
                }
            }
        }

        if (details.length() == 0) {
            details.append("Currently no tor circuits.");
        }

        circuitDetails = details.toString();
    }

}