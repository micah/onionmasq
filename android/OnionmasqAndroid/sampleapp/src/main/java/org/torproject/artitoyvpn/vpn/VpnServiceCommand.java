package org.torproject.artitoyvpn.vpn;

import static org.torproject.artitoyvpn.vpn.ArtiVpnService.ACTION_START_VPN;
import static org.torproject.artitoyvpn.vpn.ArtiVpnService.ACTION_STOP_VPN;

import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.os.Build;

public class VpnServiceCommand {

    public static void startVpn(Context context) {
        if (context == null) {
            return;
        }
        Intent intent = new Intent(context.getApplicationContext(), ArtiVpnService.class);
        intent.setAction(ACTION_START_VPN);
        startServiceIntent(context, intent);
    }



    public static void stopVpn(Context context) {
        if (context == null) {
            return;
        }
        Intent intent = new Intent(context.getApplicationContext(), ArtiVpnService.class);
        intent.setAction(ACTION_STOP_VPN);
        startServiceIntent(context, intent);
    }

    private static void startServiceIntent(Context context, Intent intent) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            } else {
                context.startService(intent);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }
}
