#[cfg(feature = "udp-tunnel")]
use smoltcp::wire::UdpPacket;
use smoltcp::wire::{
    IpAddress, IpEndpoint, Ipv6ExtHeader, Ipv6ExtHeaderRepr, Ipv6HopByHopHeader, Ipv6HopByHopRepr,
    Ipv6OptionFailureType, Ipv6OptionRepr, Ipv6Packet,
};
use smoltcp::{
    socket::tcp::Socket as TcpSocket,
    socket::tcp::SocketBuffer as TcpSocketBuffer,
    wire::{IpProtocol, Ipv4Packet, TcpPacket},
};
#[cfg(feature = "udp-tunnel")]
use std::net::SocketAddr;
use tracing::{debug, trace, warn};

use crate::device::Packet;

pub enum ParseResult {
    NewTcpSocket {
        source: IpEndpoint,
        dest: IpEndpoint,
        socket: TcpSocket<'static>,
    },
    #[cfg(feature = "udp-tunnel")]
    UdpData(crate::udp_tunnel::UdpData),
}

pub struct Parser {}

impl Parser {
    fn maybe_make_tcp_socket(packet: &TcpPacket<&[u8]>) -> Option<TcpSocket<'static>> {
        if packet.syn() && !packet.ack() {
            Some(TcpSocket::new(
                TcpSocketBuffer::new(vec![0; 4096]),
                TcpSocketBuffer::new(vec![0; 4096]),
            ))
        } else {
            None
        }
    }

    fn handle_tcp_packet(
        src_addr: IpAddress,
        dst_addr: IpAddress,
        payload: &[u8],
    ) -> Option<ParseResult> {
        match TcpPacket::new_checked(payload) {
            Ok(tcp_packet) => {
                #[cfg(feature = "very-verbose")]
                trace!(
                    "pkt tcp {}:{} -> {}:{}{}{}{}{}{}{}",
                    src_addr,
                    tcp_packet.src_port(),
                    dst_addr,
                    tcp_packet.dst_port(),
                    if tcp_packet.syn() { " syn" } else { "" },
                    if tcp_packet.ack() { " ack" } else { "" },
                    if tcp_packet.fin() { " fin" } else { "" },
                    if tcp_packet.rst() { " rst" } else { "" },
                    if tcp_packet.psh() { " psh" } else { "" },
                    if tcp_packet.urg() { " urg" } else { "" },
                );
                if let Some(mut socket) = Self::maybe_make_tcp_socket(&tcp_packet) {
                    let src_port = tcp_packet.src_port();
                    let dst_port = tcp_packet.dst_port();

                    trace!(
                        "New incoming TCP connection: {}:{} -> {}:{}",
                        src_addr,
                        src_port,
                        dst_addr,
                        dst_port
                    );

                    assert!(!socket.is_open());
                    assert!(!socket.is_active());

                    if let Err(e) = socket.listen((dst_addr, dst_port)) {
                        // This is either because it's unaddressable or already open. Either way,
                        // it's not a serious problem.
                        warn!("Failed to create listening socket: {e:?}");
                    }
                    return Some(ParseResult::NewTcpSocket {
                        source: IpEndpoint {
                            addr: src_addr,
                            port: src_port,
                        },
                        dest: IpEndpoint {
                            addr: dst_addr,
                            port: dst_port,
                        },
                        socket,
                    });
                }
            }
            Err(err) => {
                warn!("Unable to parse TCP packet: {}", err);
            }
        }
        None
    }

    #[cfg(feature = "udp-tunnel")]
    fn handle_udp_packet(
        src_addr: IpAddress,
        dst_addr: IpAddress,
        payload: &[u8],
    ) -> Option<ParseResult> {
        match UdpPacket::new_checked(payload) {
            Ok(udp_packet) => {
                #[cfg(feature = "very-verbose")]
                trace!(
                    "pkt udp {}:{} -> {}:{}",
                    src_addr,
                    udp_packet.src_port(),
                    dst_addr,
                    udp_packet.dst_port(),
                );

                // This could use std::net::IpAddr::is_global once that
                // is stable. We mostly want to filter out link-local and
                // multicast, which will include our DNS server address.
                let dst_is_global = match &dst_addr {
                    IpAddress::Ipv4(v4) => {
                        !(v4.is_link_local()
                            || v4.is_broadcast()
                            || v4.is_loopback()
                            || v4.is_multicast()
                            || v4.is_unspecified())
                    }
                    IpAddress::Ipv6(v6) => {
                        !(v6.is_link_local()
                            || v6.is_loopback()
                            || v6.is_multicast()
                            || v6.is_unspecified())
                    }
                };

                if dst_is_global && udp_packet.src_port() != 0 && udp_packet.dst_port() != 0 {
                    // Convert smoltcp to std data types here
                    let source = SocketAddr::new(src_addr.into(), udp_packet.src_port());
                    let dest = SocketAddr::new(dst_addr.into(), udp_packet.dst_port());

                    // The UDP tunnel needs an owned Vec for the data,
                    // which it will internally queue for some time. Without
                    // a reference counted input buffer, this is the point
                    // where we get stuck making a copy of the datagram.
                    return Some(ParseResult::UdpData(crate::udp_tunnel::UdpData {
                        source,
                        dest,
                        data: udp_packet.payload().to_vec(),
                    }));
                }
            }
            Err(err) => {
                warn!("Unable to parse UDP packet: {}", err);
            }
        }
        None
    }

    pub fn parse(packet: &Packet) -> Option<ParseResult> {
        let vers = packet[0] >> 4;
        let (src, dst, protocol, payload) = match vers {
            4 => {
                let pkt = match Ipv4Packet::new_checked(packet.as_slice()) {
                    Ok(v) => v,
                    Err(e) => {
                        debug!("Failed to parse IPv4 packet: {}", e);
                        return None;
                    }
                };
                (
                    IpAddress::Ipv4(pkt.src_addr()),
                    IpAddress::Ipv4(pkt.dst_addr()),
                    pkt.next_header(),
                    pkt.payload(),
                )
            }
            6 => {
                let pkt = match Ipv6Packet::new_checked(packet.as_slice()) {
                    Ok(v) => v,
                    Err(e) => {
                        debug!("Failed to parse IPv6 packet: {}", e);
                        return None;
                    }
                };

                let mut payload = pkt.payload();
                let mut next_header = pkt.next_header();

                // Sigh. IPv6 has a 'nifty' feature that lets the 'next header' actually be this
                // "hop-by-hop protocol" extension that has a bunch of custom option fields.
                // We have to imitate what smoltcp does here and parse it out to see what the
                // *real* protocol and payload are, making sure to drop packets where the
                // options header says we should drop them if we don't understand them. (Otherwise,
                // we could be tricked into opening a socket for something smoltcp will drop!)
                //
                // (look at smoltcp src/iface/interface/ipv6.rs for what it does with these)
                if let IpProtocol::HopByHop = next_header {
                    let ext_header = match Ipv6ExtHeader::new_checked(payload) {
                        Ok(v) => v,
                        Err(e) => {
                            debug!("Failed to parse IPv6 extension header: {}", e);
                            return None;
                        }
                    };
                    let ext_repr = match Ipv6ExtHeaderRepr::parse(&ext_header) {
                        Ok(v) => v,
                        Err(e) => {
                            debug!("Failed to parse IPv6 extension header: {}", e);
                            return None;
                        }
                    };

                    let hbh_hdr = match Ipv6HopByHopHeader::new_checked(ext_repr.data) {
                        Ok(v) => v,
                        Err(e) => {
                            debug!("Failed to parse IPv6 hop-by-hop header: {}", e);
                            return None;
                        }
                    };

                    let hbh_repr = match Ipv6HopByHopRepr::parse(&hbh_hdr) {
                        Ok(v) => v,
                        Err(e) => {
                            debug!("Failed to parse IPv6 hop-by-hop header: {}", e);
                            return None;
                        }
                    };

                    for opt_repr in &hbh_repr.options {
                        match opt_repr {
                            Ipv6OptionRepr::Pad1 | Ipv6OptionRepr::PadN(_) => (),
                            Ipv6OptionRepr::Unknown { type_, .. } => {
                                match Ipv6OptionFailureType::from(*type_) {
                                    Ipv6OptionFailureType::Skip => (),
                                    _ => {
                                        warn!("Encountered unknown hop-by-hop option, dropping packet!");
                                        return None;
                                    }
                                }
                            }
                            _ => {
                                warn!("Encountered unknown hop-by-hop option (2nd case), dropping packet!");
                                return None;
                            }
                        }
                    }

                    next_header = ext_repr.next_header;
                    payload = &payload[ext_repr.header_len()..];
                }

                (
                    IpAddress::Ipv6(pkt.src_addr()),
                    IpAddress::Ipv6(pkt.dst_addr()),
                    next_header,
                    payload,
                )
            }
            x => {
                debug!("Unknown IP protocol version {}", x);
                return None;
            }
        };
        if let IpProtocol::Tcp = protocol {
            return Self::handle_tcp_packet(src, dst, payload);
        }
        #[cfg(feature = "udp-tunnel")]
        if let IpProtocol::Udp = protocol {
            return Self::handle_udp_packet(src, dst, payload);
        }
        None
    }
}
