//! Additional TURN authentication support

use crate::error::Error;
use rand::Rng;
use std::time::Duration;
use stun::{
    attributes::{ATTR_NONCE, ATTR_REALM, ATTR_USERNAME},
    integrity::MessageIntegrity,
    message::{Message, Setter},
    textattrs::{Nonce, Realm, Username},
};

/// TURN authentication methods
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum TurnAuth {
    /// No authentication supported
    ///
    /// If the server requires authentication, the allocation will fail.
    ///
    None,

    /// TURN Username and password
    ///
    /// The username will be sent in cleartext. Passwords are used to compute
    /// an "integrity" digest that's included with allocate, bind, and refresh
    /// messages.
    ///
    UsernamePassword(String, String),

    /// Long-term secret, for `--use-auth-secret` in coturn
    ///
    /// This is also referred to as the "TURN REST API" style of auth, because
    /// it's intended for use cases where this secret is known to a trusted
    /// server but not to untrusted clients. The shared secret generates
    /// time-limited username/password pairs.
    ///
    Secret(String),
}

impl TurnAuth {
    /// Generate a specific username and password
    pub fn generate_credentials(&self) -> Result<(String, String), Error> {
        match self {
            TurnAuth::None => Err(Error::AuthRequired),
            TurnAuth::UsernamePassword(username, password) => {
                Ok((username.to_owned(), password.to_owned()))
            }
            TurnAuth::Secret(secret) => {
                // The timestamp is randomized somewhat, in order to avoid
                // leaking the client's precise timestamp if we don't need to.
                //
                // Typically this cannot fail; this propagates an error from
                // the underlying library, which only actually occurs if the
                // system clock is set earlier than the UNIX epoch.

                let mut rng = rand::thread_rng();
                let duration_range = (60 * 30)..(60 * 90);
                let duration = Duration::from_secs(rng.gen_range(duration_range));
                Ok(turn::auth::generate_long_term_credentials(
                    secret, duration,
                )?)
            }
        }
    }
}

/// State for an authenticated TURN session
pub struct TurnSession {
    pub nonce: Nonce,
    pub realm: Realm,
    pub username: Username,
    pub password: String,
}

impl TurnSession {
    /// Construct a TurnSession from a TurnAuth and a Message
    pub fn new(auth: &TurnAuth, message: &Message) -> Result<Self, Error> {
        let (username, password) = auth.generate_credentials()?;
        let username = Username::new(ATTR_USERNAME, username);
        let nonce = Nonce::get_from_as(message, ATTR_NONCE)?;
        let realm = Realm::get_from_as(message, ATTR_REALM)?;
        Ok(TurnSession {
            username,
            password,
            realm,
            nonce,
        })
    }

    /// Generate a MessageIntegrity for this session
    pub fn message_integrity(&self) -> MessageIntegrity {
        MessageIntegrity::new_long_term_integrity(
            self.username.text.clone(),
            self.realm.text.clone(),
            self.password.clone(),
        )
    }
}

impl Setter for TurnSession {
    fn add_to(&self, message: &mut Message) -> Result<(), stun::Error> {
        self.username.add_to(message)?;
        self.realm.add_to(message)?;
        self.nonce.add_to(message)?;
        self.message_integrity().add_to(message)?;
        Ok(())
    }
}
