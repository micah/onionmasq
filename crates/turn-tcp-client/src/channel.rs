//! Channel number and timer management

use crate::{error::Error, timers::Timers};
use std::{collections::HashMap, net::SocketAddr, time::Instant};
pub use turn::proto::channum::ChannelNumber;
use turn::proto::channum::{MAX_CHANNEL_NUMBER, MIN_CHANNEL_NUMBER};

/// Information about one channel within an allocation
///
/// Like allocations, channel bindings have a refresh timer. We don't maintain
/// a separate expiration timer, because channel bindings can always be
/// re-established when necessary without an additional round-trip.
/// We don't need to treat refreshing an expired vs. un-expired binding any
/// differently.
///
#[derive(Clone, Debug)]
pub struct ChannelInfo {
    pub number: ChannelNumber,
    pub timers: Timers,
}

impl ChannelInfo {
    /// Construct info for a brand new channel binding
    pub(crate) fn new(number: ChannelNumber, now: Instant) -> Self {
        Self {
            number,
            timers: Timers::for_binding(now),
        }
    }
}

/// Manage the set of all TURN Channels, within an Allocation
///
pub(crate) struct ChannelMap {
    channels_by_peer: HashMap<SocketAddr, ChannelInfo>,
    peers_by_channel: HashMap<ChannelNumber, SocketAddr>,
    next_channel_number: ChannelNumber,
}

impl ChannelMap {
    /// Construct a new empty channel map
    pub(crate) fn new() -> Self {
        Self {
            channels_by_peer: HashMap::new(),
            peers_by_channel: HashMap::new(),
            next_channel_number: ChannelNumber(MIN_CHANNEL_NUMBER),
        }
    }

    /// Check whether a [`ChannelNumber`] is in use, expiring it if necessary.
    fn channel_in_use_or_expire(&mut self, now: Instant, channel_number: ChannelNumber) -> bool {
        let peer = match self.peers_by_channel.get(&channel_number) {
            None => return false,
            Some(peer) => *peer,
        };
        match self.channels_by_peer.get(&peer) {
            None => unreachable!(),
            Some(channel) => {
                assert_eq!(channel_number, channel.number);
                if !channel.timers.is_expired(now) {
                    return true;
                }
            }
        };
        // Forget the expired channel
        self.channels_by_peer.remove(&peer);
        self.peers_by_channel.remove(&channel_number);
        false
    }

    /// Find an unused [`ChannelNumber`]
    ///
    /// Every time we perform this search, the `next_channel_number` advances
    /// and we begin at that point searching for an unused number. Slots that
    /// contain expired bindings will be garbage collected as they are visited.
    ///
    /// Returns [`Errors::TooManyPeers`] if there are too many distinct
    /// non-expired peer addresses to represent as a TURN ChannelNumber.
    ///
    pub(crate) fn find_unused_channel(&mut self, now: Instant) -> Result<ChannelNumber, Error> {
        let num_usable_numbers = MAX_CHANNEL_NUMBER - MIN_CHANNEL_NUMBER + 1;
        for _ in 0..num_usable_numbers {
            let channel_number = self.next_channel_number;
            self.next_channel_number = ChannelNumber(if channel_number.0 == MAX_CHANNEL_NUMBER {
                MIN_CHANNEL_NUMBER
            } else {
                channel_number.0 + 1
            });
            if !self.channel_in_use_or_expire(now, channel_number) {
                return Ok(channel_number);
            }
        }
        Err(Error::TooManyPeers)
    }

    /// Remember to refresh all bindings and the allocation next time we use them
    ///
    /// This must visit every binding to set its refresh timer. We take that
    /// opportunity to also garbage collect any bindings that are already expired.
    ///
    pub(crate) fn refresh_all_and_collect_expired(&mut self, now: Instant) {
        self.channels_by_peer.retain(|_peer, channel| {
            if channel.timers.is_expired(now) {
                // Forget the expired channel
                self.peers_by_channel.remove(&channel.number);
                false
            } else {
                // Keep this binding, and mark it for refresh
                channel.timers.set_refresh_now(now);
                true
            }
        });
    }

    /// Look up the ChannelInfo for a particular peer, if one exists.
    ///
    /// This does not take any actions based on the channel's timers.
    ///
    pub(crate) fn get_channel_by_peer(&self, peer: SocketAddr) -> Option<&ChannelInfo> {
        self.channels_by_peer.get(&peer)
    }

    /// Remember a new or refreshed channel binding.
    ///
    pub(crate) fn insert_or_refresh(&mut self, peer: SocketAddr, info: ChannelInfo) {
        let number = info.number;
        self.channels_by_peer.insert(peer, info);
        self.peers_by_channel.insert(number, peer);
    }
}
