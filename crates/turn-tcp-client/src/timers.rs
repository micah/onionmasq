//! Allocation and Binding refresh timing
//!
//! We do not maintain active timers with the async runtime, instead choosing
//! to check timeouts only when we are otherwise using the allocation/binding.
//! If a binding or allocation is not used, we need to additionally be able to
//! record its status as expired. Expired allocations must be recreated explicitly.
//! Expired bindings can be recreated without special effort, but we keep track
//! of their expiration in case we want to recycle channel numbers.
//!

use crate::error::Error;
use std::time::{Duration, Instant};
use stun::message::{Getter, Message};
use turn::proto::lifetime::Lifetime;

/// RFC8656 says bindings last for 10 minutes.
const BINDING_LIFETIME: Duration = Duration::from_secs(60 * 10);

/// Timestamps for allocation or binding refresh
#[derive(Clone, Debug)]
pub struct Timers {
    expire_at: Instant,
    refresh_at: Instant,
}

impl Timers {
    /// Initialize the timers for a new allocation, from the reply message
    ///
    /// When the server-provided expiration time comes up, the Allocation will
    /// begin returning [`Error::AllocationExpired`] and it will need to be
    /// rebuilt.
    ///
    /// We will begin trying to refresh the allocation when half the lifetime
    /// has elapsed.
    ///
    pub(crate) fn for_allocation_reply(now: Instant, message: &Message) -> Result<Self, Error> {
        let mut lifetime: Lifetime = Default::default();
        lifetime.get_from(message)?;
        Ok(Timers {
            expire_at: now + lifetime.0,
            refresh_at: now + lifetime.0 / 2,
        })
    }

    /// Initialize from a new channel binding
    ///
    /// Bindings have a fixed lifetime. Unlike allocations, bindings are very
    /// cheap to refresh. Refreshing a new binding vs. an expired binding are
    /// no different from the client's perspective.
    ///
    /// For bindings, expiration time is used to garbage collect peers that
    /// have reusable ChannelNumbers. This should be a time at which we know
    /// the server has definitely already expired our channel number.
    ///
    /// This sets the 'refresh' timer to half the actual lifetime,
    /// and 'expire' to twice that.
    ///
    pub(crate) fn for_binding(now: Instant) -> Self {
        Timers {
            expire_at: now + BINDING_LIFETIME * 2,
            refresh_at: now + BINDING_LIFETIME / 2,
        }
    }

    /// Does this allocation or binding need to be refreshed yet?
    pub fn needs_refresh(&self, now: Instant) -> bool {
        now >= self.refresh_at
    }

    /// Has this allocation or binding expired?
    pub fn is_expired(&self, now: Instant) -> bool {
        now >= self.expire_at
    }

    /// Begin refreshing as soon as possible
    pub(crate) fn set_refresh_now(&mut self, now: Instant) {
        self.refresh_at = now;
    }

    /// When is this allocation or binding scheduled to expire?
    pub fn expire_at(&self) -> Instant {
        self.expire_at
    }

    /// When is this allocation or binding scheduled for a refresh?
    pub fn refresh_at(&self) -> Instant {
        self.refresh_at
    }
}
