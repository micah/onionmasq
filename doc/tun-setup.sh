# Simple example setup for `onionmasq` which routes all traffic
# on the machine through the VPN except Tor connectivity, using
# the 16-bit fwmark code added by onionmasq.
#

set -ve 

SUDO=sudo
BIN=./onionmasq
DEV=onion0
TABLE=3628
MARK=$((0xc185))

$SUDO setcap cap_net_admin=ep $BIN

$SUDO ip tuntap add name $DEV mode tun user $USER
$SUDO ip link set $DEV up

$SUDO ip addr add 169.254.42.127/24 dev $DEV
$SUDO ip addr add fe80::127/96 dev $DEV

# Add new default routes that only work when the MARK is unset.
# Everything except Arti's own outgoing traffic will be sent to the tun device.
$SUDO ip route add default via 169.254.42.128 dev $DEV table $TABLE
$SUDO ip route add default via fe80::128 dev $DEV table $TABLE
$SUDO ip rule add not fwmark $MARK table $TABLE

# Use a globally routable resolver inside and outside the VPN
echo nameserver 1.1.1.1 | $SUDO tee /etc/resolv.conf

# Alternatively you could use the fake DNS provided by onionmasq, but this
# must be done in some way that does not effect onionmasq itself, for example
# by using a chroot or container. Provided here only for example.
#
# echo nameserver 169.254.42.53 | $SUDO tee /etc/resolv.conf


